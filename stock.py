# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
import datetime
from trytond.pyson import Eval


class StockMove(metaclass=PoolMeta):
    __name__ = 'stock.move'

    planned_date_icon = fields.Function(
        fields.Char("Icon"),
        'get_planned_date_icon')
    icon = fields.Function(
        fields.Char("Icon"),
        'get_icon')
    confirmed_planned_date = fields.Boolean(
        "Confirmed planned date",
        states={'readonly': Eval('state').in_(['cancel', 'assigned', 'done'])},
        depends=['state'])
    requested_quantity = fields.Function(
        fields.Float(
            "Requested quantity",
            digits=(16, Eval('unit_digits', 2)),
            depends=['unit_digits']),
        'get_requested_quantity')
    remaining_quantity = fields.Function(
        fields.Float(
            "Remaining quantity",
            digits=(16, Eval('unit_digits', 2)),
            depends=['unit_digits']),
        'get_requested_quantity')
    purchase_purpose = fields.Function(
        fields.Char("Purpose"),
        'get_purchase_purpose', searcher='search_purchase_purpose')

    @classmethod
    def search_purchase_purpose(cls, name, clause):
        return [('origin.requests.purpose',) + tuple(clause[1:]) + ('purchase.line',)]

    def get_icon(self, name):
        Date = Pool().get('ir.date')
        if self.planned_date and self.planned_date <= Date.today() and\
                self.planned_date > Date.today() - datetime.timedelta(days=7):
            return 'warning'
        if self.planned_date:
            return 'tick' if self.planned_date >= Date.today() else 'cross'

    def get_planned_date_icon(self, name):
        return ''

    def get_purchase_line(move):
        PurchaseLine = Pool().get('purchase.line')
        return move.origin if isinstance(move.origin, PurchaseLine) else None

    def get_requested_quantity(self, name):
        purchase_line = StockMove.get_purchase_line(self)
        return purchase_line.quantity if purchase_line else float(0)

    def get_remaining_quantity(self, name):
        purch_line = StockMove.get_purchase_line(self)
        return purch_line._move_remaining_quantity if purch_line else float(0)

    def get_purchase_purpose(self, name):
        purchase_line = StockMove.get_purchase_line(self)
        if purchase_line and purchase_line.requests:
            return ', '.join([req.purpose if req.purpose else '' for req in self.origin.requests])
